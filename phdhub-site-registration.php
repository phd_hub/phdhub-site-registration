<?php
/* 
Plugin Name: PhD Hub Site Registration
Plugin URI: https://it.auth.gr
Description: Site registrations plugin for the PhD Hub platform
Text Domain: phdhub-sr
Author: Ioannis Zachros
Author URI: https://it.auth.gr
License: GPLv2 or later
*/

defined('ABSPATH') or die;

/*
 * Enable plugins, themes and add settings for every new local hub
 */
 add_action('wpmu_new_blog', 'create_new_site',10);
 function create_new_site($blog_id) {
	switch_to_blog($blog_id);
	
	/* ACTIVATE THIRD PARTY PLUGINS */
	activate_plugin('admin-category-search/admin-category-search.php');
	activate_plugin('all-in-one-wp-security-and-firewall/wp-security.php');
	activate_plugin('delete-me/delete-me.php');
	activate_plugin('expire-passwords/expire-passwords.php');
	activate_plugin('favorites/favorites.php');
	activate_plugin('gdpr-data-request-form/gdpr-data-request-form.php');
	activate_plugin('google-sitemap-generator/sitemap.php');
	activate_plugin('openid/openid.php');
	activate_plugin('super-socializer/super_socializer.php');
	activate_plugin('two-factor-authentication/two-factor-login.php');
	activate_plugin('user-role-editor/user-role-editor.php');
	activate_plugin('wp-user-avatar/wp-user-avatar.php');	
	
	/* ACTIVATE CUSTOM PLUGINS */	
	switch_theme('phdhub_theme');
		
	/* UPDATE OPTIONS */
	$aio_wp_security_settings = json_decode(file_get_contents(dirname(__FILE__) . '/config/settings/aio-wp-security.json'), true);	
	$simplefavorites_dependencies_settings = json_decode(file_get_contents(dirname(__FILE__) . '/config/settings/simplefavorites-dependencies.json'), true);
	$simplefavorites_users_settings = json_decode(file_get_contents(dirname(__FILE__) . '/config/settings/simplefavorites-users.json'), true);
	$simplefavorites_display_settings = json_decode(file_get_contents(dirname(__FILE__) . '/config/settings/simplefavorites-display.json'), true);
	$sm_options_settings = json_decode(file_get_contents(dirname(__FILE__) . '/config/settings/sm-options.json'), true);
	$the_champ_general_settings = json_decode(file_get_contents(dirname(__FILE__) . '/config/settings/the-champ-general.json'), true);
	$the_champ_counter_settings = json_decode(file_get_contents(dirname(__FILE__) . '/config/settings/the-champ-counter.json'), true);
	$the_champ_login_settings = json_decode(file_get_contents(dirname(__FILE__) . '/config/settings/the-champ-login.json'), true);
	$the_champ_sharing_settings = json_decode(file_get_contents(dirname(__FILE__) . '/config/settings/the-champ-sharing.json'), true);
	$the_champ_facebook_settings = json_decode(file_get_contents(dirname(__FILE__) . '/config/settings/the-champ-facebook.json'), true);
	
	update_option('aio_wp_security_configs', $aio_wp_security_settings);	
	update_option('simplefavorites_dependencies', $simplefavorites_dependencies_settings);
	update_option('simplefavorites_users', $simplefavorites_users_settings);
	update_option('simplefavorites_display', $simplefavorites_display_settings);
	update_option('sm_options', $sm_options_settings);
	update_option('the_champ_general', $the_champ_general_settings);
	update_option('the_champ_counter', $the_champ_counter_settings);
	update_option('the_champ_login', $the_champ_login_settings);
	update_option('the_champ_sharing', $the_champ_sharing_settings);
	update_option('the_champ_facebook', $the_champ_facebook_settings);
		
	restore_current_blog();
 }
 add_action('wpmu_new_blog', 'phdhub_add_default_languages',11);
 function phdhub_add_default_languages($blog_id) {
    switch_to_blog($blog_id);
    global $polylang, $wp_settings_errors;
    //fix for polylang validation error
    $wp_settings_errors = [];
    $adminModel = new PLL_Admin_Model($polylang->options);
    
    $args		 = array(
	'name'		 => "English",
	'slug'		 => "en",
	'locale'	 => "en_GB",
	'flag'		 => "gb",
	'rtl'		 => 0,
	'term_group'	 => 0
    );
    
    $adminModel->add_language( $args );    
    restore_current_blog();
}
